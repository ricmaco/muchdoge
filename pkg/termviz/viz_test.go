package termviz

import (
	"fmt"
	"image"
	"net/http"
	"testing"
)

var (
	url string = "https://i.redd.it/tce2w6tcjy681.jpg"
	img image.Image
)

func TestMain(m *testing.M) {
	res, _ := http.Get(url)
	img, _, _ = image.Decode(res.Body)

	m.Run()
}

func TestToASCII(t *testing.T) {
	display := ToASCII(img)
	fmt.Println(display)
	if len(display) == 0 {
		t.Error("shown image is empty")
	}
}
