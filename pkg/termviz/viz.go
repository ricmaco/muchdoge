package termviz

import (
	"fmt"
	"image"

	"github.com/qeesung/image2ascii/convert"
)

var (
	converter *convert.ImageConverter = convert.NewImageConverter()
	options   convert.Options         = convert.DefaultOptions
)

func init() {
	options.FitScreen = false
	options.Ratio = .1
}

// ToASCII returns the provided image as string of ANSI colored characters
func ToASCII(image image.Image) string {
	return converter.Image2ASCIIString(image, &options)
}

// Display prints the provided image as string of ANSI colored characters
func Display(image image.Image) {
	fmt.Println(ToASCII(image))
}
