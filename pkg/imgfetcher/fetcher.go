package imgfetcher

import (
	"errors"
	"image"
	"net/http"

	_ "image/jpeg"
	_ "image/png"
)

var (
	ErrInvalidUrl error = errors.New("invalid image url")
)

// FetchImage fetches the image contained at provided url
func FetchImage(url string) (image.Image, error) {
	if len(url) == 0 {
		return nil, ErrInvalidUrl
	}

	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	image, _, err := image.Decode(res.Body)
	if err != nil {
		return nil, err
	}

	return image, nil
}

// FetchImages fetches the images contained at provided urls
func FetchImages(urls []string) ([]image.Image, error) {
	var images []image.Image

	for _, url := range urls {
		image, err := FetchImage(url)
		if err != nil {
			return images, nil
		}

		images = append(images, image)
	}

	return images, nil
}
