package imgfetcher

import "testing"

var (
	urls []string = []string{
		"https://i.redd.it/tce2w6tcjy681.jpg",
		"https://i.redd.it/u7s2n1tf6y681.jpg",
		"https://i.redd.it/3mibkyr8nx681.jpg",
	}
)

func TestFetchImage(t *testing.T) {
	image, err := FetchImage(urls[0])
	if err != nil {
		t.Error("unable to fetch image")
	}

	if image == nil {
		t.Error("expected an images, got nil")
	}
}

func TestFetchImages(t *testing.T) {
	wantImagesLen := 3

	images, err := FetchImages(urls)
	if err != nil {
		t.Error("unable to fetch images")
	}

	if len(images) != wantImagesLen {
		t.Errorf("expected %d images, got %d", wantImagesLen, len(images))
	}
}
