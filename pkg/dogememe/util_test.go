package dogememe

import (
	"encoding/json"
	"testing"
)

var (
	body = []byte(`{
		"total": 8268,
		"count": 10,
		"data": [
			{
				"submission_id": "rlnpjb",
				"submission_url": "https://i.redd.it/tce2w6tcjy681.jpg",
				"submission_title": "😔",
				"permalink": "/r/dogecoin/comments/rlnpjb/_/",
				"author": "OhHayThereU",
				"created": "2021-12-21T20:42:28",
				"timestamp": "2021-12-21T20:45:03.981000"
			},
			{
				"submission_id": "rlm694",
				"submission_url": "https://i.redd.it/u7s2n1tf6y681.jpg",
				"submission_title": "Time to play some Crypto Kong!",
				"permalink": "/r/dogecoin/comments/rlm694/time_to_play_some_crypto_kong/",
				"author": "Agitated_Bend_5441",
				"created": "2021-12-21T19:30:00",
				"timestamp": "2021-12-21T19:45:03.034000"
			},
			{
				"submission_id": "rljw04",
				"submission_url": "https://i.redd.it/3mibkyr8nx681.jpg",
				"submission_title": "Had to fix my girlfriend’s grandmother’s letter blocks. Happy Holidays and hold strong 💪",
				"permalink": "/r/dogecoin/comments/rljw04/had_to_fix_my_girlfriends_grandmothers_letter/",
				"author": "Grizzlybehrz",
				"created": "2021-12-21T17:42:25",
				"timestamp": "2021-12-21T17:45:03.486000"
			},
			{
				"submission_id": "rljp38",
				"submission_url": "https://i.redd.it/ls7b2rsklx681.gif",
				"submission_title": "I made this: \"Understanding the stock market with gifs.\"",
				"permalink": "/r/dogecoin/comments/rljp38/i_made_this_understanding_the_stock_market_with/",
				"author": "Dogecoin_Mememaster",
				"created": "2021-12-21T17:33:06",
				"timestamp": "2021-12-21T18:00:03.249000"
			},
			{
				"submission_id": "rlj7oa",
				"submission_url": "https://i.redd.it/6rbqedzjhx681.jpg",
				"submission_title": "HODL",
				"permalink": "/r/dogecoin/comments/rlj7oa/hodl/",
				"author": "Secure-Initial1670",
				"created": "2021-12-21T17:10:42",
				"timestamp": "2021-12-21T17:15:03.401000"
			},
			{
				"submission_id": "rlihe2",
				"submission_url": "https://i.redd.it/1hj41ornbx681.jpg",
				"submission_title": "And if you hadn't realized yet it's Taco Tuesday, pick up enough for your family and friends.",
				"permalink": "/r/dogecoin/comments/rlihe2/and_if_you_hadnt_realized_yet_its_taco_tuesday/",
				"author": "Agitated_Bend_5441",
				"created": "2021-12-21T16:37:29",
				"timestamp": "2021-12-21T16:45:03.365000"
			},
			{
				"submission_id": "rli2h5",
				"submission_url": "https://i.redd.it/6t01gdt68x681.jpg",
				"submission_title": "How are all of you Donkey Shibes hodling out today?",
				"permalink": "/r/dogecoin/comments/rli2h5/how_are_all_of_you_donkey_shibes_hodling_out_today/",
				"author": "Agitated_Bend_5441",
				"created": "2021-12-21T16:18:02",
				"timestamp": "2021-12-21T16:30:03.423000"
			},
			{
				"submission_id": "rlgy10",
				"submission_url": "https://i.redd.it/8a0m1ifuyw681.jpg",
				"submission_title": "Snoo Doge will terminate boredom.",
				"permalink": "/r/dogecoin/comments/rlgy10/snoo_doge_will_terminate_boredom/",
				"author": "Agitated_Bend_5441",
				"created": "2021-12-21T15:25:39",
				"timestamp": "2021-12-21T15:45:03.504000"
			},
			{
				"submission_id": "rlgw7v",
				"submission_url": "https://i.redd.it/tik68nleyw681.png",
				"submission_title": "Merry Early Christmas Shibes, share this with your friends and family!",
				"permalink": "/r/dogecoin/comments/rlgw7v/merry_early_christmas_shibes_share_this_with_your/",
				"author": "DogemarineAlt",
				"created": "2021-12-21T15:23:16",
				"timestamp": "2021-12-21T15:30:03.385000"
			},
			{
				"submission_id": "rlfpx0",
				"submission_url": "https://i.redd.it/h1ifffgsnw681.jpg",
				"submission_title": "Time for a romp in the snow.",
				"permalink": "/r/dogecoin/comments/rlfpx0/time_for_a_romp_in_the_snow/",
				"author": "Agitated_Bend_5441",
				"created": "2021-12-21T14:23:42",
				"timestamp": "2021-12-21T14:45:03.073000"
			}
		]
	}`)
	response Response
)

func TestMain(m *testing.M) {
	json.Unmarshal(body, &response)
	m.Run()
}

func TestGetImageUrls(t *testing.T) {
	wantUrlsLen := 9

	urls, err := GetImagesUrls(response.Data)
	if err != nil {
		t.Error("invalid image found")
	}

	if len(urls) != wantUrlsLen {
		t.Errorf("found %d url, want %d", len(urls), wantUrlsLen)
	}
}

func TestHasSupportedImagesOK(t *testing.T) {
	imageUrl := response.Data[0].Url

	if !HasSupportedImage(imageUrl) {
		t.Errorf("%s contains a supported image", imageUrl)
	}
}

func TestHasSupportedImagesKO(t *testing.T) {
	imageUrl := response.Data[3].Url

	if HasSupportedImage(imageUrl) {
		t.Errorf("%s contains an unsupported image", imageUrl)
	}
}
