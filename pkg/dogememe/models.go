package dogememe

type Submission struct {
	Id        string `json:"submission_id"`
	Url       string `json:"submission_url"`
	Title     string `json:"submission_title"`
	Permalink string `json:"permalink"`
	Author    string `json:"author"`
	Created   string `json:"created"`
}

type Response struct {
	Total int          `json:"total"`
	Count int          `json:"count"`
	Data  []Submission `json:"data"`
}
