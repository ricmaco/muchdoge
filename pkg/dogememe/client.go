package dogememe

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Config struct {
	Url   string
	Limit int
	Skip  int
}

// Fetch fetches a list of Doge memes submissions.
// This method only supports the API at https://api.doge-meme.lol/v1/memes/.
func Fetch(config *Config) (Response, error) {
	var response Response

	completeUrl := fmt.Sprintf("%s?skip=%d&limit=%d", config.Url, config.Skip,
		config.Limit)

	res, err := http.Get(completeUrl)
	if err != nil {
		return response, err
	}

	bodyDecoder := json.NewDecoder(res.Body)
	if err = bodyDecoder.Decode(&response); err != nil {
		return response, err
	}

	return response, nil
}
