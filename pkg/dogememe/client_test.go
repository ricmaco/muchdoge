package dogememe

import (
	"testing"
)

func TestFetch(t *testing.T) {
	config := &Config{
		"https://api.doge-meme.lol/v1/memes/",
		10,
		0,
	}

	res, err := Fetch(config)
	if err != nil {
		t.Error("error launching fetch")
	}

	if res.Count == 0 || len(res.Data) == 0 {
		t.Error("parsed json is invalid")
	}
}
