package dogememe

import (
	"errors"
	"strings"
)

var (
	ErrInvalidUrl error = errors.New("invalid image url")
)

// GetImagesUrls extracts the images links from a list of submissions
func GetImagesUrls(submissions []Submission) ([]string, error) {
	var urls []string

	for _, submission := range submissions {
		if len(submission.Url) == 0 {
			return urls, ErrInvalidUrl
		}

		if !HasSupportedImage(submission.Url) {
			continue
		}

		urls = append(urls, submission.Url)
	}

	return urls, nil
}

// HasSupportedImage checks if the provided url links to a supported image
func HasSupportedImage(s string) bool {
	return strings.HasSuffix(s, ".jpg") ||
		strings.HasSuffix(s, "png")
}
