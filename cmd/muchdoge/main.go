package main

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"gitlab.com/ricmaco/muchdoge/pkg/dogememe"
	"gitlab.com/ricmaco/muchdoge/pkg/imgfetcher"
	"gitlab.com/ricmaco/muchdoge/pkg/termviz"
)

const (
	url     = "https://api.doge-meme.lol/v1/memes/"
	results = 100
	retries = 3
)

func main() {
	config := &dogememe.Config{
		Url:   url,
		Limit: results,
	}

	var urls []string
	r := 0
	for len(urls) == 0 && r < retries {
		r++

		response, err := dogememe.Fetch(config)
		if err != nil {
			continue
		}

		urls, err = dogememe.GetImagesUrls(response.Data)
		if err != nil {
			continue
		}
	}

	if len(urls) > 0 {
		rand.Seed(time.Now().Unix())
		u := urls[rand.Intn(len(urls))]

		image, _ := imgfetcher.FetchImage(u)

		fmt.Printf("Displaying %s\n", u)
		termviz.Display(image)

		return
	}

	log.Fatalf("error: unable to fetch any images from %s api\n", url)
}
