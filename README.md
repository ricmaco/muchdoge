
![logo](images/logo.png)

# muchdoge

Simple utility to fetch a random meme image from [/r/dogecoin](https://www.reddit.com/r/dogecoin) and display it in your terminal in ASCIIart.

## Running Tests

To run tests, run the following command

```bash
  go test ./...
```

## Build

The easiest way to build this project is to install `make`.

Go to the root of the project, where the `Makefile` is, and launch:

```bash
  make clean build
```

The executable is generated in `build/muchdoge`.
    
## Contributing

This is just a two-evenings project without any kind of interest in clean code or well-built architecture.

Nonetheless contributions are always welcome! Just put the code in a merge request and it will be reviewed it and eventually merged.

## License

[MIT](https://choosealicense.com/licenses/mit/)