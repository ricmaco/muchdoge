GO = go
GOARCH = amd64
GOOS = linux

OUT = build

build: muchdoge

%: cmd/%/*.go create-out
	${GO} build -o ${OUT}/$@ $<

create-out:
	mkdir -p ${OUT}

clean:
	-${RM} -r ${OUT}